package fr.istic.pit.backend.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Classe configuration Spring : documentation OpenAPI + Swagger
 */
@Configuration
public class OpenAPIConfig {

    /**
     * Clé de propriété contenant la version de l'API
     */
    public static final String API_VERSION_PROP_KEY = "pit-backend.api.version";

    /**
     * Placeholder Spring pour la récupération de la propriété de version de l'API
     */
    public static final String API_VERSION_PROPERTY = "${" + API_VERSION_PROP_KEY + ":0.0.0}";

    /**
     * Configuration de base de la documentation d'API
     *
     * @param apiVersion Version de l'API
     * @return La configuration OpenAPI
     */
    @Bean
    public OpenAPI customOpenAPI(
        @Value(API_VERSION_PROPERTY) String apiVersion) {
        return new OpenAPI()
            .components(new Components()
                .addSecuritySchemes("basicScheme", new SecurityScheme()
                    .type(SecurityScheme.Type.HTTP)
                    .scheme("basic")
                )
            )
            .info(new Info()
                .title("Documentation de l'API REST")
                .description("Documentation OpenAPI 3 de l'API Restful fourni par le backend du projet PIT.")
                .version(apiVersion)
            );
    }

}
