package fr.istic.pit.backend.controllers.errors;

import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Cette classe est un adaptateur permettant de modéliser
 * les détails d'une erreur de validation à partir du bean renvoyé par Spring
 * lors de la validation.
 */
public class ValidationErrorDetails {

    /**
     * L'indice de l'argument sujet à la validation
     */
    public final int argumentIndex;

    /**
     * Le nom de la méthode dans laquelle est survenue l'erreur de validation
     */
    public final String boundMethod;

    /**
     * La liste des erreurs par champs de l'objet n'ayant pas passé la validation
     */
    public final List<ErrorFieldDetails> fieldErrors;

    /**
     * Constructeur de base
     *
     * @param ex L'exception de validation levée par Spring
     * @see MethodArgumentNotValidException
     */
    public ValidationErrorDetails(MethodArgumentNotValidException ex) {
        this.argumentIndex = ex.getParameter().getParameterIndex();
        this.boundMethod = ex.getParameter().getExecutable().toGenericString();
        this.fieldErrors = Collections.unmodifiableList(
            ex.getBindingResult().getFieldErrors().stream()
                .map(ErrorFieldDetails::new)
                .collect(Collectors.toList())
        );
    }

    /**
     * Affichage de l'erreur, pour journalisation
     *
     * @return Le descriptif textuel de l'erreur
     */
    public String toString() {
        StringBuilder message = new StringBuilder("\n*** Validation exception ***\n" +
            "Argument index :\t" + argumentIndex + "\n" +
            "Bound method :\t" + boundMethod + "\n" +
            "Field errors :\n");

        fieldErrors.forEach(message::append);
        return message.toString();
    }

    /**
     * Classe interne modélisant une erreur de validation au niveau d'un champ.
     * Elle joue également le rôle d'adaptateur pour la classe Spring FieldError
     *
     * @see FieldError
     */
    public static class ErrorFieldDetails {

        /**
         * Le nom du champ invalide
         */
        public final String field;

        /**
         * La valeur rejetée
         */
        public final String rejectedValue;

        /**
         * Le code descriptif de l'erreur de validation
         */
        public final String code;

        /**
         * Le message d'erreur
         */
        public final String message;

        /**
         * Constructeur de base
         *
         * @param fe L'erreur de validation de champ créée par Spring
         */
        public ErrorFieldDetails(FieldError fe) {
            this.field = fe.getField();
            this.rejectedValue = Optional.ofNullable(fe.getRejectedValue()).map(Object::toString).orElse(null);
            this.code = fe.getCode();
            this.message = fe.getDefaultMessage();
        }

        @Override
        public String toString() {
            return "\t\t - Field '" +
                field +
                "' : rejected value: [" +
                rejectedValue +
                "], code : [" +
                code +
                "] with the message : \"" +
                message +
                "\".\n";
        }
    }
}
