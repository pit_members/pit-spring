package fr.istic.pit.backend.controllers.stubs;

import fr.istic.pit.backend.utils.Utils;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Bouchon pour le SIG (Système d'Information Géographique)
 * Répond à la route /sig par la liste des points d'intérêts fixes
 * stockés dans le fichier JSON &lt;resources&gt;/stubs/sig_stub_data.json
 */
@RestController
@RequestMapping("/sig")
@ConditionalOnProperty("pit-backend.sig.stub.enable")
@Hidden
public class SIGStubController {
    private static final Logger logger = LoggerFactory.getLogger(SIGStubController.class);

    /**
     * Emplacement du fichier des données du bouchon
     */
    public static final String SIG_STUB_DATA = "classpath:values/sig_stub_data.json";

    @Value(SIG_STUB_DATA)
    Resource sigDataResource;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @Operation(description = "Récupère la liste des points d'intérêts fixes (données en dur)")
    public String getPoints() {
        try {
            return Utils.readResource(sigDataResource);
        } catch (Exception e) {
            logger.warn("The SIG stub could not load its data because :", e);
            return "";
        }
    }
}
