package fr.istic.pit.backend.repositories;

import fr.istic.pit.backend.model.entities.DronePoint;

/**
 * Repository pour les points de mission drone
 */
public interface DronePointRepository extends EntityRepository<DronePoint, Long> {
}
