package fr.istic.pit.backend.repositories;

import fr.istic.pit.backend.model.entities.Resource;

import java.util.List;

/**
 * Repository des moyens
 */
public interface ResourceRepository extends EntityRepository<Resource, Long> {

    /**
     * Trouver les moyens aux états macro et micro spécifiés.
     *
     * @param macroState L'état macro des moyens recherchés
     * @param microState L'état micro des moyens recherchés
     * @return Les moyens trouvés
     */
    List<Resource> findAllByMacroStateAndMicroState(String macroState, String microState);

    /**
     * Trouver les moyens à l'état macro spécifiée.
     *
     * @param macroState L'état macro des moyens recherchés
     * @return Les moyens trouvés
     */
    List<Resource> findAllByMacroState(String macroState);

    /**
     * Trouver les moyens à l'état micro spécifié.
     *
     * @param microState L'état micro des moyens recherchés
     * @return Les moyens trouvés
     */
    List<Resource> findAllByMicroState(String microState);
}
