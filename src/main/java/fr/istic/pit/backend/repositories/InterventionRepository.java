package fr.istic.pit.backend.repositories;

import fr.istic.pit.backend.model.entities.Intervention;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Repository pour les interventions
 */
public interface InterventionRepository extends EntityRepository<Intervention, Long> {
    @Query("SELECT i FROM Intervention i ORDER BY i.id ASC")
    List<Intervention> findAll();
}
