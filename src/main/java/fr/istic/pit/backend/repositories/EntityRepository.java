package fr.istic.pit.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository de base
 *
 * @param <T>  Type de l'entité
 * @param <ID> Type du champ identifiant de l'entité
 */
@NoRepositoryBean
public interface EntityRepository<T, ID> extends
    JpaRepository<T, ID>,
    JpaSpecificationExecutor<T>,
    PagingAndSortingRepository<T, ID> {

}
