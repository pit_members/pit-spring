package fr.istic.pit.backend.model.state;

import fr.istic.pit.backend.model.entities.Resource;

import static java.time.LocalDateTime.now;

/**
 * Classe statique modélisant la machine à état macro pour la situation des moyens
 * à l'échelle de l'intervention
 */
public final class MacroState {

    /*
     * Constantes
     */
    public static final String WAITING = "WAITING";
    public static final String COMING = "COMING";
    public static final String ARRIVED = "ARRIVED";
    public static final String REJECTED = "REJECTED";
    public static final String CANCELLED = "CANCELLED";
    public static final String RELEASED = "RELEASED";
    public static final String END = "END"; // Retro-compatibilité Bdd
    public static final String[] FINAL_STATES = new String[]{REJECTED, RELEASED, CANCELLED, END};

    /**
     * Classe abstraite modélisant un état de base
     */
    public abstract static class State {
        /**
         * Le moyen qui possède l'état
         */
        protected final Resource resource;

        protected State(Resource resource) {
            this.resource = resource;
        }

        public String id() {
            return getClass().getSimpleName().toUpperCase();
        }
    }

    /**
     * État initial
     */
    public static class Start extends State {
        public Start(Resource resource) {
            super(resource);
            resource.setRequestTime(now());
            resource.setMicroState(MicroState.MOVING);
        }

        public Waiting request() {
            return new Waiting(resource);
        }

        public Coming create() {
            return new Coming(resource);
        }
    }

    /**
     * État macro « En attente »
     */
    public static class Waiting extends State {
        public Waiting(Resource resource) {
            super(resource);
            resource.setName(null);
        }

        public Coming accept() {
            return new Coming(resource);
        }

        public Cancelled cancel() {
            return new Cancelled(resource);
        }

        public Rejected reject() {
            return new Rejected(resource);
        }
    }

    /**
     * État macro « En trajet »
     */
    public static class Coming extends State {
        public Coming(Resource resource) {
            super(resource);
            resource.setStartingTime(now());
        }

        public Arrived arrive() {
            resource.setArrivalTime(now());
            return new Arrived(resource);
        }

        public Released release() {
            return new Released(resource);
        }
    }

    /**
     * État macro « Arrivée SSL »
     */
    public static class Arrived extends State {
        public Arrived(Resource resource) {
            super(resource);
        }

        public Released release() {
            return new Released(resource);
        }
    }

    /**
     * État macro « Libéré »
     */
    public static class Released extends End {
        public Released(Resource resource) {
            super(resource);
        }
    }

    /**
     * État macro « Rejeté »
     */
    public static class Rejected extends End {
        public Rejected(Resource resource) {
            super(resource);
        }
    }

    /**
     * État macro « Annulé »
     */
    public static class Cancelled extends End {
        public Cancelled(Resource resource) {
            super(resource);
        }
    }


    /**
     * État terminal
     */
    public static abstract class End extends State {
        public End(Resource resource) {
            super(resource);
            resource.setReleaseTime(now());
            resource.setMicroState(MicroState.END);
            resource.setLatitude(null);
            resource.setLongitude(null);
        }
    }

    MacroState() {
    }
}
