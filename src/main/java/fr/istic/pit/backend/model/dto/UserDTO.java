package fr.istic.pit.backend.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * DTO représentant un utilisateur dans l'application
 */
@Valid
@Schema(
    name = "Utilisateur",
    description = "Les identifiants de connexion")
public class UserDTO {

    /**
     * Le nom d'utilisateur/login
     */
    @NotEmpty
    @Schema(description = "Le login de l'utilisateur")
    public String login;

    /**
     * Le mot de passe
     */
    @NotEmpty
    @Schema(description = "Le mot de passe de l'utilisateur")
    public String password;

}
