package fr.istic.pit.backend.model.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

/**
 * DTO représentant les moyens
 */
@Valid
@Schema(
    name = "Moyen",
    description = "La resource modélisant un moyen"
)
@JsonInclude(NON_NULL)
public class ResourceDTO {

    /**
     * Identifiant technique du moyen
     */
    @Schema(description = "Identifiant technique de la ressource", accessMode = READ_ONLY)
    public Long id;

    /**
     * Le nom/identifiant du moyen
     */
    @Schema(description = "Le nom, identifiant ou matricule du moyen, attribué par le CODIS", accessMode = READ_ONLY)
    public String name;

    /**
     * Latitude
     */
    @Schema(description = "La latitude du point d'intérêt, au format décimal",
        accessMode = READ_ONLY, type = "number")
    public Double latitude;

    /**
     * Longitude
     */
    @Schema(description = "La longitude du point d'intérêt, au format décimal",
        accessMode = READ_ONLY, type = "number")
    public Double longitude;

    /**
     * Le rôle du point (POI / moyen)
     */
    @Schema(description = "Le rôle du point d'intérêt. \n **Code couleurs** : \n" +
        "<table>" +
        "    <thead>" +
        "        <tr>" +
        "            <th>Code</th>" +
        "            <th>Couleur</th>" +
        "            <th>Signification</th>" +
        "        </tr>" +
        "    </thead>" +
        "    <tbody>" +
        "        <tr>" +
        "            <td>0</td>" +
        "            <td>noir</td>" +
        "            <td>Cheminement (accès, rocades, pénétrantes). <br/>Couleur de remplacement par défaut</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>1</td>" +
        "            <td>rouge</td>" +
        "            <td>Lié à l’incendie</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>2</td>" +
        "            <td>orange</td>" +
        "            <td>Lié aux « risques particuliers »</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>3</td>" +
        "            <td>vert</td>" +
        "            <td>Lié aux personnes</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>4</td>" +
        "            <td>bleu</td>" +
        "            <td>Lié à l’eau</td>" +
        "        </tr>" +
        "        <tr>" +
        "            <td>5</td>" +
        "            <td>violet</td>" +
        "            <td>Lié au commandement, à la sectorisation</td>" +
        "        </tr>" +
        "    </tbody>" +
        "</table>")
    public Integer role;

    /**
     * Un code identifiant le type de pictogramme à utiliser
     */
    @NotEmpty
    @Schema(
        description = "Le code identifiant le type de moyen demandé",
        allowableValues = {"VSAV", "FPT", "VLCG"}
    )
    public String pictoType;

    /**
     * L'id de l'intervention
     */
    @Schema(description = "L'identifiant technique de l'intervention",
        accessMode = READ_ONLY, type = "number")
    public long intervId;

    /**
     * L'heure de la demande
     */
    @Schema(description = "L'heure de la demande", accessMode = READ_ONLY)
    public LocalDateTime requestTime;

    /**
     * L'heure de départ
     */
    @Schema(description = "L'heure de départ", accessMode = READ_ONLY)
    public LocalDateTime startingTime;

    /**
     * L'heure d'arrivée
     */
    @Schema(description = "L'heure d'arrivée", accessMode = READ_ONLY)
    public LocalDateTime arrivalTime;

    /**
     * L'heure de libération
     */
    @Schema(description = "L'heure de libération", accessMode = READ_ONLY)
    public LocalDateTime releaseTime;

    /**
     * État macro du moyen
     */
    @Schema(description = "L'état macro du moyen",
        accessMode = READ_ONLY,
        allowableValues = {"WAITING", "COMING", "ARRIVED", "END"}
    )
    public String macroState;

    /**
     * État micro du moyen
     * (en trajet, sur place)
     */
    @Schema(description = "L'état micro du moyen",
        accessMode = READ_ONLY,
        allowableValues = {"MOVING", "IDLE", "END"}
    )
    public String microState;

    /**
     * Indique si le moyen est en attente dans le CRM
     */
    @Schema(description = "Indique si le moyen est en attente dans le CRM (pas encore d'emplacement attribué)",
        accessMode = READ_ONLY, type = "boolean")
    public boolean crm;
}
