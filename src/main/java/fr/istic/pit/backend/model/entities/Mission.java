package fr.istic.pit.backend.model.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

import static java.util.Objects.requireNonNull;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entité stockant une mission, qui est une suite de points géographiques
 * suivis par le drone.
 */
@Entity
public class Mission {
    /**
     * Identifiant technique de la mission
     */
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    /**
     * L'intervention concernée par la mission
     */
    @ManyToOne
    private Intervention intervention;

    /**
     * Le point sur lequel est situé/se rend le drone
     */
    private Long currentPointId;

    /**
     * Indique si la mission se joue en boucle (true) ou en aller/retour (false)
     */
    private boolean missionLoop;

    /**
     * La liste des points de la mission
     */
    @OneToMany(mappedBy = "mission", cascade = CascadeType.ALL)
    @OrderBy("orderNumber asc")
    private final Collection<DronePoint> points = new ArrayList<>();

    /**
     * Indique si la mission est terminée
     */
    private boolean terminated;

    public Mission() {
    }

    private Mission(Builder builder) {
        setId(builder.id);
        setIntervention(builder.intervention);
        setCurrentPointId(builder.currentPointId);
        setTerminated(builder.terminated);
        setMissionLoop(builder.missionLoop);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Intervention getIntervention() {
        return intervention;
    }

    public void setIntervention(Intervention intervention) {
        this.intervention = intervention;
    }

    public Long getCurrentPointId() {
        return currentPointId;
    }

    public void setCurrentPointId(Long currentPointId) {
        this.currentPointId = currentPointId;
    }

    public boolean isMissionLoop() {
        return missionLoop;
    }

    public void setMissionLoop(boolean missionLoop) {
        this.missionLoop = missionLoop;
    }

    public Collection<DronePoint> getPoints() {
        return points;
    }

    public void addDronePoint(DronePoint point) {
        requireNonNull(point);
        if (points.contains(point))
            return;

        points.add(point);
        point.setMission(this);
    }

    public void removeDronePoint(DronePoint point) {
        if (!points.contains(point))
            return;

        points.remove(point);
        point.setMission(null);
    }

    public boolean isTerminated() {
        return terminated;
    }

    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    /**
     * Builder
     */
    public static final class Builder {
        private Long id;
        private Intervention intervention;
        private Long currentPointId;
        private boolean terminated;
        private boolean missionLoop;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder intervention(Intervention val) {
            intervention = val;
            return this;
        }

        public Builder currentPointId(Long val) {
            currentPointId = val;
            return this;
        }

        public Builder terminated(boolean val) {
            terminated = val;
            return this;
        }

        public Builder missionLoop(boolean val) {
            missionLoop = val;
            return this;
        }

        public Mission build() {
            return new Mission(this);
        }
    }

    /**
     * Fusionne les propriétés de la mission source dans l'objet courant.
     *
     * @param source L'objet source
     */
    public void merge(Mission source) {
        requireNonNull(source, "Source mission object is null");
        setId(source.id);
        setIntervention(source.intervention);
        setCurrentPointId(source.currentPointId);
        setTerminated(source.terminated);
        setMissionLoop(source.missionLoop);
    }
}
