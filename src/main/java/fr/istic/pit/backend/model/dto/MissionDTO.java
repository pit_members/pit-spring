package fr.istic.pit.backend.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.istic.pit.backend.services.mqtt.drone.model.DroneState;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.*;
import static io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY;

/**
 * Une mission est une suite de points géographiques
 * suivis par le drone.
 * DTO servant à la consultation.
 */
@Valid
@JsonInclude(NON_NULL)
@Schema(
    name = "Mission",
    description = "Représente une mission pour le drone, constituée d'une séquence de points"
)
public class MissionDTO {
    /**
     * Identifiant technique de la mission
     */
    @Schema(description = "Identifiant technique du point", accessMode = READ_ONLY)
    public Long id;

    /**
     * L'intervention concernée par la mission
     */
    @Schema(
        description = "L'identifiant technique de l'intervention à laquelle se rattache la mission",
        type = "number",
        accessMode = READ_ONLY
    )
    public Long intervId;

    /**
     * Le point sur lequel est situé/se rend le drone
     */
    @Schema(
        description = "L'identifiant technique du point sur lequel se trouve ou se rend le drone",
        type = "number",
        accessMode = READ_ONLY
    )
    @JsonInclude(ALWAYS)
    public Long currentPointId;

    /**
     * La liste des points de la mission
     */
    @JsonInclude(NON_EMPTY)
    @Schema(description = "La liste des points constituant la mission")
    public Collection<DronePointDTO> points = new ArrayList<>();

    /**
     * Le dernier état connu du drone
     */
    public DroneState lastDroneState;

    /**
     * La mission est-elle terminée ?
     */
    public boolean terminated;

    /**
     * Indique si la mission se joue en boucle (true) ou en aller-retour
     */
    @Schema(description = "Le mode de déroulé de la mission (true = boucle, false = A/R)",
        type = "boolean", accessMode = READ_ONLY)
    public boolean missionLoop;
}
