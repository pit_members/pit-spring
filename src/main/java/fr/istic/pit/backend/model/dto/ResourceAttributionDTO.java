package fr.istic.pit.backend.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * DTO stockant les informations nécessaires à l'attribution d'un moyen.
 */
@Valid
@Schema(
    name = "Attribution d'un moyen",
    description = "Les informations nécessaires au CODIS pour attribuer un moyen à une intervention"
)
public class ResourceAttributionDTO {
    /**
     * Le nom/identifiant à attribuer au moyen
     */
    @Schema(description = "Le nom, identifiant ou matricule du moyen, à attribuer pour le nouveau moyen déployé")
    @NotEmpty
    public String name;
}
