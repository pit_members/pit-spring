package fr.istic.pit.backend.model.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

import static java.util.Objects.requireNonNull;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Entité stockant un point sur lequel se rend le drone.
 */
@Entity
public class DronePoint {
    /**
     * Identifiant technique du point
     */
    @GeneratedValue(strategy = IDENTITY)
    @Id
    private Long id;

    /**
     * La mission à laquelle se rapporte le point
     */
    @ManyToOne
    private Mission mission;

    /**
     * Numéro d'ordre du point dans la mission
     */
    private Integer orderNumber;

    /**
     * Latitude du point
     */
    private Double latitude;

    /**
     * Longitude du point
     */
    private Double longitude;

    /**
     * Altitude du point (relative au niveau de la mer)
     */
    private Double altitude;

    public DronePoint() {
    }

    private DronePoint(Builder builder) {
        setId(builder.id);
        setMission(builder.mission);
        setOrderNumber(builder.orderNumber);
        setLatitude(builder.latitude);
        setLongitude(builder.longitude);
        setAltitude(builder.altitude);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission newMission) {
        if (Objects.equals(mission, newMission))
            return;

        Mission oldMission = mission;
        mission = newMission;

        if (oldMission != null)
            oldMission.removeDronePoint(this);

        if (newMission != null)
            newMission.addDronePoint(this);
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    /**
     * Builder
     */
    public static final class Builder {
        private Long id;
        private Mission mission;
        private int orderNumber;
        private Double latitude;
        private Double longitude;
        private Double altitude;

        public Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder mission(Mission val) {
            mission = val;
            return this;
        }

        public Builder orderNumber(int val) {
            orderNumber = val;
            return this;
        }

        public Builder latitude(Double val) {
            latitude = val;
            return this;
        }

        public Builder longitude(Double val) {
            longitude = val;
            return this;
        }

        public Builder altitude(Double val) {
            altitude = val;
            return this;
        }

        public DronePoint build() {
            return new DronePoint(this);
        }
    }

    /**
     * Fusionne les propriétés du point source dans l'objet courant.
     *
     * @param source L'objet source
     */
    public void merge(DronePoint source) {
        requireNonNull(source, "source drone point object is null");
        setId(source.id);
        setMission(source.mission);
        setOrderNumber(source.orderNumber);
        setLatitude(source.latitude);
        setLongitude(source.longitude);
        setAltitude(source.altitude);
    }
}
