package fr.istic.pit.backend.services.mqtt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashSet;
import java.util.Set;

import static java.util.concurrent.Executors.newSingleThreadExecutor;
import static org.eclipse.paho.client.mqttv3.MqttConnectOptions.MQTT_VERSION_3_1_1;

/**
 * Classe abstraite pour la définition d'un service de base d'émission et de réception
 * de messages MQTT.
 */
public abstract class AbstractMqttService {

    protected final Logger LOG = LoggerFactory.getLogger(getClass());

    /**
     * URL du broker par défaut
     */
    protected static final String DEFAULT_BROKER_URL = "tcp://localhost:1883";

    /**
     * Les options qui seront utilisées pour toutes les connexions
     */
    protected final MqttConnectOptions connectOptions;

    /**
     * Le client MQTT
     */
    private MqttAsyncClient client;

    /**
     * L'URL du broker de messages MQTT
     */
    @Value("${pit-backend.mqtt.url:" + DEFAULT_BROKER_URL + "}")
    protected String brokerUrl;

    /**
     * Le topic de base (préfixe) à utiliser
     */
    protected final String baseTopic;

    /**
     * La QoS des messages émis
     */
    @Value("${pit-backend.mqtt.qos:2}")
    protected int messageQos;

    /**
     * Le mapper JSON à utiliser pour les messages MQTT
     */
    protected final ObjectMapper mapper;

    /**
     * L'ensemble des topics auxquels le service est inscrit
     */
    private final Set<String> subscribedTopics;

    /**
     * Constructeur de base d'un service MQTT
     *
     * @param mapper    Le mapper JSON à utiliser pour les messages MQTT
     * @param baseTopic Le topic de base du client MQTT
     */
    protected AbstractMqttService(ObjectMapper mapper, String baseTopic) {
        this.mapper = mapper;
        this.baseTopic = baseTopic;
        this.connectOptions = connectOptions();
        subscribedTopics = new HashSet<>();
    }

    @PostConstruct
    public final void initialize() throws MqttException {
        client = new MqttAsyncClient(brokerUrl, generateClientId(), persistence());
        client.setCallback(callback());
        client.setBufferOpts(disconnectedBufferOptions());
        serviceInitialized();
        newSingleThreadExecutor().submit(this::clientConnect);
    }

    /**
     * Callback rappelé lorsque le service est initialisé
     */
    protected abstract void serviceInitialized();

    private MqttCallback callback() {
        // Callback du client MQTT
        return new MqttCallbackExtended() {
            @Override
            public void connectionLost(Throwable cause) {
                err("Connection with broker lost because : ", cause);
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {
                LOG.debug("Message received from broker {}, topic {} : {}", brokerUrl, topic, message);
                try {
                    onMessageReceived(message, topic);
                } catch (Exception ex) {
                    err("An error occurred in the message receive callback : ", ex);
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                LOG.debug("Delivery complete : {}", token);
            }

            @Override
            public void connectComplete(boolean reconnect, String brokerUri) {
                if (reconnect) {
                    LOG.info("Reconnected to broker {}", brokerUri);
                    subscribedTopics.forEach(AbstractMqttService.this::clientSubscribe);
                } else
                    LOG.info("Connected to broker {}", brokerUri);
            }
        };
    }

    /**
     * La méthode rappelée lorsqu'un message est reçu par le client
     *
     * @param message Le message reçu
     * @param topic   Le topic de provenance
     */
    protected abstract void onMessageReceived(MqttMessage message, String topic);

    private void clientConnect() {
        // Tentative de connexion du client
        try {
            client.connect(connectOptions);
        } catch (MqttException e) {
            err("Could not connect to broker " + brokerUrl, e);
        }
    }

    /**
     * Définit le type de persistence utilisée.
     *
     * @return Une instance de MqttClientPersistence
     * @see MqttClientPersistence
     */
    protected MqttClientPersistence persistence() {
        // persistence en mémoire par défaut
        return new MemoryPersistence();
    }

    /**
     * Définit la façon dont est généré l'id unique du client
     *
     * @return Une chaîne correspondant à l'id du client
     */
    protected String generateClientId() {
        return MqttAsyncClient.generateClientId();
    }

    /**
     * Permet de définir les options pour le tampon de déconnexion.
     * Ce tampon sauvegarde les messages à envoyer lors d'une déconnexion,
     * puis est vidé par le client qui diffuse de nouveau tous les messages
     * lorsque la connexion au broker est rétablie.
     *
     * @return Une instance de DisconnectedBufferOptions
     * @see DisconnectedBufferOptions
     */
    protected DisconnectedBufferOptions disconnectedBufferOptions() {
        DisconnectedBufferOptions bufferOpts = new DisconnectedBufferOptions();
        bufferOpts.setBufferEnabled(true);
        bufferOpts.setBufferSize(100);
        bufferOpts.setPersistBuffer(false);
        bufferOpts.setDeleteOldestMessages(true);
        return bufferOpts;
    }

    /**
     * Permet de définir les options de connexion du client.
     *
     * @return Une instance de MqttConnectOptions
     * @see MqttConnectOptions
     */
    protected MqttConnectOptions connectOptions() {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        options.setAutomaticReconnect(true);
        options.setMqttVersion(MQTT_VERSION_3_1_1);
        return options;
    }

    /**
     * Publie un message sur le topic indiqué
     *
     * @param topic   Le topic sur lequel le publier
     * @param message Le message à publier
     */
    public final void publishMessage(String topic, Object message) {
        try {
            byte[] data = mapper.writeValueAsBytes(message);

            if (!client.isConnected()) {
                LOG.warn("Client is disconnected, waiting for reconnection... {} messages buffered.",
                    client.getBufferedMessageCount());
            }
            client.publish(topic, data, messageQos, false);

        } catch (JsonProcessingException e) {
            err("Could not generate event message because : ", e);
        } catch (MqttException e) {
            err("Could not publish event message because : ", e);
        }
    }

    protected final void err(String message, Throwable ex) {
        // Journalisation de l'erreur
        LOG.error(message + " : {}", ex.toString());
        LOG.debug("----- Exception root cause -----\n", ex);
    }

    @PreDestroy
    public final void release() {
        try {
            client.disconnect();
        } catch (MqttException ex) {
            err("Could not disconnect from the broker at " + brokerUrl, ex);
        }
    }

    /**
     * Abonne le client à un topic
     *
     * @param topic Le topic sur lequel s'inscrire
     */
    public final void subscribe(String topic) {
        if (subscribedTopics.add(topic))
            clientSubscribe(topic);
    }

    /**
     * Désabonne le client d'un topic
     *
     * @param topic Le topic duquel se désinscrire
     */
    public final void unsubscribe(String topic) {
        if (topic == null || !subscribedTopics.contains(topic))
            return;

        clientUnsubscribe(topic);
    }

    private void clientUnsubscribe(String topic) {
        // Désinscription du client du topic
        try {
            if (client.isConnected()) {
                client.unsubscribe(topic);
                LOG.debug("Unsubscribed from {}", topic);
            }
        } catch (MqttException e) {
            err("Could not unsubscribe from the topic {} " + topic +
                " on the broker at {} " + brokerUrl + " because ", e);
        }
    }

    private void clientSubscribe(String topic) {
        // Inscription du client au topic
        try {
            if (client.isConnected()) {
                client.subscribe(topic, messageQos);
                LOG.debug("Subscribed to {}", topic);
            }
        } catch (MqttException e) {
            err("Could not subscribe to topic {} " + topic +
                " on the broker at {} " + brokerUrl + " because ", e);
        }
    }
}
