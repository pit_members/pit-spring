package fr.istic.pit.backend.services.mappers;

import fr.istic.pit.backend.model.dto.UserDTO;
import fr.istic.pit.backend.model.entities.User;
import org.springframework.stereotype.Service;

/**
 * Mapper pour les utilisateurs
 */
@Service
public class UserMapper implements Mapper<User, UserDTO> {
    @Override
    public UserDTO map(User source) {
        UserDTO dto = new UserDTO();
        dto.login = source.getLogin();
        dto.password = source.getPassword();
        return dto;
    }

    @Override
    public User reverseMap(UserDTO target) {
        User entity = new User();
        entity.setLogin(target.login);
        entity.setPassword(target.password);
        return entity;
    }
}
