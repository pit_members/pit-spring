package fr.istic.pit.backend.services.mqtt.drone.model;

import fr.istic.pit.backend.services.mqtt.AbstractEvent;

/**
 * Commande du drone
 */
public class DroneCommand extends AbstractEvent<DroneCommandType> {

}
