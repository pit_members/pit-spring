package fr.istic.pit.backend.services.mqtt.drone.model;

public enum DroneEventType {
    /**
     * Le drone est arrivé au point
     */
    ARRIVE_TO_POINT(true, false),

    /**
     * Le drone a décollé
     */
    ARM_DONE(true, false),

    /**
     * Le drone envoie son état
     */
    DRONE_STATE(true, true),

    /**
     * Le drone a atterri
     */
    RTL_DONE(false, true),

    /**
     * Le drone a dû effectuer un atterrissage d'urgence
     */
    EMERGENCY_RTL(false, true);

    /**
     * Indique si l'évènement est lié au déroulé de la mission.
     */
    private final boolean missionRelated;

    /**
     * Indique si l'évènement est extérieur doit être interprété peu importe la situation dans la mission
     */
    private final boolean external;

    DroneEventType(boolean missionRelated, boolean external) {
        this.missionRelated = missionRelated;
        this.external = external;
    }

    public boolean isMissionRelated() {
        return missionRelated;
    }

    public boolean isExternal() {
        return external;
    }
}
