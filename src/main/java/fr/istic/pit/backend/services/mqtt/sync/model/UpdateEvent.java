package fr.istic.pit.backend.services.mqtt.sync.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.istic.pit.backend.services.mqtt.AbstractEvent;

import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * Évènement de mise à jour (synchronisation)
 */
@JsonInclude(NON_NULL)
public final class UpdateEvent extends AbstractEvent<UpdateEventType> {

    /**
     * Créé un évènement de mise à jour
     *
     * @param eventType Le type d'évènement
     * @param eventTime La date/heure d'émission
     * @param eventNo   Le numéro d'évènement
     */
    public UpdateEvent(UpdateEventType eventType, LocalDateTime eventTime, long eventNo) {
        this.eventTime = eventTime;
        this.eventNo = eventNo;
        this.eventType = eventType;
    }
}
