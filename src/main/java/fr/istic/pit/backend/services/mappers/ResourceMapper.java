package fr.istic.pit.backend.services.mappers;

import fr.istic.pit.backend.model.dto.ResourceDTO;
import fr.istic.pit.backend.model.entities.Resource;
import org.springframework.stereotype.Service;

/**
 * Mapper pour les moyens
 */
@Service
public class ResourceMapper implements Mapper<Resource, ResourceDTO> {

    @Override
    public ResourceDTO map(Resource source) {
        ResourceDTO dto = new ResourceDTO();

        dto.latitude = source.getLatitude();
        dto.longitude = source.getLongitude();
        dto.id = source.getId();
        dto.role = source.getRole();
        dto.pictoType = source.getPictoType();
        dto.startingTime = source.getStartingTime();
        dto.arrivalTime = source.getArrivalTime();
        dto.releaseTime = source.getReleaseTime();
        dto.requestTime = source.getRequestTime();
        dto.macroState = source.getMacroState();
        dto.microState = source.getMicroState();
        dto.name = source.getName();
        dto.crm = source.isInCrm();
        dto.intervId = source.getIntervention().getId();

        return dto;
    }

    @Override
    public Resource reverseMap(ResourceDTO target) {
        Resource entity = new Resource();

        entity.setLatitude(target.latitude);
        entity.setLongitude(target.longitude);
        entity.setRole(target.role);
        entity.setPictoType(target.pictoType);
        entity.setName(target.name);

        return entity;
    }
}
