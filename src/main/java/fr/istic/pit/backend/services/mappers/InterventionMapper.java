package fr.istic.pit.backend.services.mappers;

import fr.istic.pit.backend.model.dto.InterventionDTO;
import fr.istic.pit.backend.model.entities.Intervention;
import org.springframework.stereotype.Service;

/**
 * Mapper pour les interventions.
 */
@Service
public class InterventionMapper implements Mapper<Intervention, InterventionDTO> {

    /**
     * Le mapper pour les moyens
     */
    private final ResourceMapper resourceMapper;

    public InterventionMapper(ResourceMapper resourceMapper) {
        this.resourceMapper = resourceMapper;
    }

    @Override
    public InterventionDTO map(Intervention entity) {
        InterventionDTO dto = new InterventionDTO();
        dto.id = entity.getId();
        dto.informations = entity.getInformations();
        dto.latitude = entity.getLatitude();
        dto.longitude = entity.getLongitude();
        dto.startTime = entity.getStartTime();
        dto.endTime = entity.getEndTime();
        dto.droneAvailability = printEnum(entity.getDroneAvailability());
        dto.sinisterCode = entity.getSinisterCode();
        dto.name = entity.getName();
        return dto;
    }

    @Override
    public Intervention reverseMap(InterventionDTO dto) {
        Intervention entity = new Intervention.Builder()
            .informations(dto.informations)
            .latitude(dto.latitude)
            .longitude(dto.longitude)
            .name(dto.name)
            .sinisterCode(dto.sinisterCode)
            .build();
        resourceMapper.reverseMapList(dto.resources)
            .forEach(entity::addResource);
        return entity;
    }
}
