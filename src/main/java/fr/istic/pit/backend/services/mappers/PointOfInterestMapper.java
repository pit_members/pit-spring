package fr.istic.pit.backend.services.mappers;

import fr.istic.pit.backend.model.dto.PointOfInterestDTO;
import fr.istic.pit.backend.model.entities.PointOfInterest;
import org.springframework.stereotype.Service;

/**
 * Mapper pour les points d'intérêts
 */
@Service
public class PointOfInterestMapper implements Mapper<PointOfInterest, PointOfInterestDTO> {

    @Override
    public PointOfInterestDTO map(PointOfInterest source) {
        PointOfInterestDTO dto = new PointOfInterestDTO();

        dto.latitude = source.getLatitude();
        dto.longitude = source.getLongitude();
        dto.id = source.getId();
        dto.pictoType = source.getPictoType();
        dto.intervId = source.getIntervention().getId();

        return dto;
    }

    @Override
    public PointOfInterest reverseMap(PointOfInterestDTO target) {
        PointOfInterest entity = new PointOfInterest();

        entity.setLatitude(target.latitude);
        entity.setLongitude(target.longitude);
        entity.setPictoType(target.pictoType);

        return entity;
    }
}
