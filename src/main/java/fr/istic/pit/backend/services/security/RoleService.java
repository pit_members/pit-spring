package fr.istic.pit.backend.services.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Boolean.parseBoolean;
import static java.util.Collections.singletonList;

/**
 * Service chargé de l'identification du rôle de l'utilisateur
 * en fonction d'un entête fourni avec les requêtes.
 * Il agit comme un filtre sur les requêtes, intercepte l'en-tête recherché et
 * déclenche l'authentificatin dans le mécanisme de Spring Security.
 */
@Service
public class RoleService extends OncePerRequestFilter {

    /**
     * Clé de l'en-tête indiquant le rôle
     */
    public static final String HEADER_ROLE = "X-PIT-Role-Codis";

    /**
     * Valeur du role du CODIS
     */
    private static final String ROLE_CODIS = "ROLE_CODIS";

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        // Récupération du booléen dans l'en-tête (false si non présent)
        boolean hasRoleCodis = parseBoolean(request.getHeader(HEADER_ROLE));

        // Si le rôle CODIS est présent
        if (hasRoleCodis) {
            // Authentification avec l'utilisateur CODIS
            Authentication auth = new UsernamePasswordAuthenticationToken("codis", "",
                singletonList(new SimpleGrantedAuthority(ROLE_CODIS)));

            SecurityContextHolder.getContext().setAuthentication(auth);
        } else {
            // Pas d'authentification
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Indique si la requête provient de l'utilisateur ayant le rôle CODIS
     * Header X-PIT-Role-Codis = TRUE
     *
     * @return true si la requête courante a été envoyée par le CODIS, false sinon
     */
    public boolean isFromCodis() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null && auth.getAuthorities().stream()
            .anyMatch(authority -> authority.getAuthority().equalsIgnoreCase(ROLE_CODIS));
    }
}
