package fr.istic.pit.backend.services.mqtt.sync;

import fr.istic.pit.backend.services.mqtt.sync.model.UpdateEvent;
import fr.istic.pit.backend.services.mqtt.sync.model.UpdateEventType;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Aspect permettant d'intercepter les appels de WebService déclenchant
 * des mises à jour de données auprès de l'API, et d'appeler le service de synchro pour émettre
 * les notifications de mise à jour correspondante.
 */
@Aspect
@Service
@SuppressWarnings("rawtypes")
public class RestInterceptionAspect {

    /**
     * Service d'émission d'évènements de synchronisation
     */
    private final SynchroEventPublisher eventPublisher;

    /**
     * Compteur pour le numéro d'évènement
     */
    private final AtomicLong eventCounter;

    /**
     * Compteur pour le nombre de requêtes reçues
     */
    private final AtomicLong requestCounter;

    /**
     * Constructeur de base
     *
     * @param eventPublisher Service d'émission d'évènements de synchronisation (dépendance)
     */
    public RestInterceptionAspect(SynchroEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
        eventCounter = new AtomicLong(0);
        requestCounter = new AtomicLong(0);
    }

    /**
     * Intercepter toutes les méthodes des contrôleurs
     */
    @Pointcut(
        "execution(* fr.istic.pit.backend.controllers.*Controller.*(..))")
    public void controllerMethods() {
    }

    /**
     * Intercepter toutes les méthodes annotées par les annotations de mapping Spring
     */
    @Pointcut(
        "@annotation(org.springframework.web.bind.annotation.GetMapping) || " +
            "@annotation(org.springframework.web.bind.annotation.PostMapping) || " +
            "@annotation(org.springframework.web.bind.annotation.DeleteMapping) || " +
            "@annotation(org.springframework.web.bind.annotation.PutMapping) || " +
            "@annotation(org.springframework.web.bind.annotation.DeleteMapping)"
    )
    public void springWebMappingAnnotations() {
    }

    /**
     * Appelée à chaque requête REST reçue
     */
    @After("controllerMethods() && springWebMappingAnnotations()")
    public void afterApiRouteCalled() {
        requestCounter.incrementAndGet();
    }

    /**
     * Récupère le nombre de requêtes reçues par l'API depuis le
     * serveur a démarré
     *
     * @return Le nombre de requêtes reçues
     */
    public long getRequestCount() {
        return requestCounter.longValue();
    }

    /**
     * Advice : appeler après que chaque méthode de contrôleur
     * annotée par @EmitUpdateEvent ait renvoyé son résultat
     *
     * @param joinPoint point de jonction
     * @param response  Réponse du contrôleur (ResponseEntity)
     * @see EmitUpdateEvent
     */
    @AfterReturning(
        pointcut = "controllerMethods() && @annotation(EmitUpdateEvent)",
        returning = "response"
    )
    public void afterRestEndpointCalled(final JoinPoint joinPoint, Object response) {
        // La requête REST interceptée a retournée une erreur : pas d'émission de messages
        if (!isAccepted(response))
            return;

        // Récupération des informations et construction du message
        UpdateEventType eventType = getEventTypeFromJoinPoint(joinPoint);
        long eventNo = eventCounter.incrementAndGet();
        LocalDateTime eventTime = LocalDateTime.now();

        UpdateEvent event = new UpdateEvent(eventType, eventTime, eventNo);

        // Envoi au topic approprié en fonction du type d'évènement
        if (eventType.isGlobal())
            eventPublisher.publishEvent(event);

        if (eventType.isPerInterv()) {
            long intervId = getInterventionIdFromJoinPoint(joinPoint);
            eventPublisher.publishEvent(event, intervId);
        }
    }

    /**
     * Récupère le type d'évènement à partir de l'annotation @EmitUpdateEvent présente sur
     * la méthode interceptée
     *
     * @param joinPoint Le point de jonction fourni par Spring AOP
     * @return Le type d'évènement paramétré sur la méthode
     */
    private UpdateEventType getEventTypeFromJoinPoint(final JoinPoint joinPoint) {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        EmitUpdateEvent annotation = method.getAnnotation(EmitUpdateEvent.class);

        return annotation.value();
    }

    /**
     * Récupère l'id de l'intervention concernée par la modification à partir
     * du premier argument passé à la méthode (paramètre intervId)
     *
     * @param joinPoint Le point de jonction fourni par Spring AOP
     * @return L'id de l'intervention
     */
    private long getInterventionIdFromJoinPoint(final JoinPoint joinPoint) {
        try {
            /* Ne dois pas arriver : la signature des méthodes interceptées
             * doit impérativement comporter un <code>long</code> comme premier argument */
            if (joinPoint.getArgs().length == 0)
                throw new IndexOutOfBoundsException();

            return (long) joinPoint.getArgs()[0];
        } catch (Exception ex) {
            return 0;
        }
    }

    /**
     * Vérifie que le code de retour de la réponse envoyée par la méthode du contrôleur
     * soit bien un code 2xx
     *
     * @param response L'objet réponse récupérée par Spring AOP
     * @return true si l'appel à la méthode du contrôleur s'est bien passée, false sinon
     */
    private boolean isAccepted(Object response) {
        if (!(response instanceof ResponseEntity))
            return false;

        int status = ((ResponseEntity) response).getStatusCodeValue();
        return status == 200 || status == 201 || status == 204;
    }
}
