package fr.istic.pit.backend.utils;

import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.EnumSet;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Classes contenant des fonctions utilitaires
 */
public class Utils {

    /**
     * Applique une fonction à l'ensemble des éléments d'une séquence itérable,
     * puis créer une nouvelle séquence pour y stocker les résultats.
     *
     * @param sourceList      La séquence source
     * @param listConstructor Le constructeur de la séquence cible
     * @param transformer     La fonction à appliquer à chaque élément
     * @param <S>             Le type des éléments sources
     * @param <T>             Le type des éléments cibles
     * @param <C>             Le type de collection cibles (inféré à partir de T)
     * @return Une liste contenant le résultat de l'application de la fonction
     */
    public static <S, T, C extends Collection<? super T>> C mapList(Iterable<? extends S> sourceList,
                                                                    Supplier<? extends C> listConstructor,
                                                                    Function<? super S, ? extends T> transformer) {
        C result = listConstructor.get();
        if (sourceList != null) {
            for (S source : sourceList) {
                result.add(transformer.apply(source));
            }
        }
        return result;
    }

    /**
     * Lit le contenu d'un fichier dans les ressources du projet
     * et le retourne sous forme de chaîne de caractères
     *
     * @param resource L'identifiant de la ressource (Spring)
     * @return La chaîne de caractère du contenu du fichier
     * @throws IOException Une erreur est survenue lors de la lecture du fichier
     */
    public static String readResource(Resource resource) throws IOException {
        try (BufferedReader reader = new BufferedReader(
            new InputStreamReader(resource.getInputStream()))) {
            return reader.lines()
                .collect(Collectors.joining("\n"));
        }
    }

    /**
     * Indique si la constante fait partie du type énumération.
     *
     * @param enumType      La classe de l'énumération (E.class)
     * @param constant      La chaîne de caractères contenant la constante d'énumération
     * @param caseSensitive si true, la comparaison est sensible à la casse
     * @param <E>           Le type d'énumération
     * @return false si la constante n'a pas été trouvée ou si le type de la constante est nul, true sinon.
     */
    public static <E extends Enum<E>> boolean isConstantOf(Class<E> enumType, String constant, boolean caseSensitive) {
        BiPredicate<String, String> predicate = caseSensitive ? String::equals : String::equalsIgnoreCase;
        return enumType != null && EnumSet.allOf(enumType)
            .stream()
            .map(Enum::name)
            .anyMatch(e -> predicate.test(e, constant));
    }
}
