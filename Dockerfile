FROM maven:3.5-jdk-8-slim as maven

COPY ./pom.xml ./pom.xml
RUN mvn dependency:go-offline -B
COPY ./src ./src
RUN mvn package -P container -DskipTests

FROM openjdk:8-jdk-slim

EXPOSE 8080
RUN mkdir -p /app/
RUN mkdir -p /app/logs/
WORKDIR /app
COPY --from=maven target/pit-spring-*.jar ./app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/app.jar"]


#VOLUME /tmp

